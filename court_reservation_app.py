from reservations_manager import ReservationsManager


class CourtReservationApp():
    def __init__(self) -> None:
        self.manager = ReservationsManager()
        self.status = True

    def run_app(self):
        while self.status == True:
            user_input = input(
                "Type what you want to do (type help for more information): ")
            match user_input:
                case "help":
                    print("Available actions:")
                    print("1- make a reservation")
                    print("2- cancel a reservation")
                    print("3- print a schedule")
                    print("4- save schedule to a file")
                    print("5- exit")
                case "1":
                    name, start_time = self.ask_user()
                    self.make_a_reservation(name, start_time)
                case "2":
                    self.cancel_reservation()
                case "3":
                    self.print_schedule()
                case "4":
                    self.save_schedule()
                case "5":
                    self.status = False

    def ask_user(self):
        name = input("Please type your first and last name: ")
        start_time = input("When would you like to book? {DD.MM.YYYY HH:MM}: ")
        return name, start_time

    def make_a_reservation(self, name, start_time):
        message, message2 = self.manager.check_conditions_before_reservation(
            name, start_time)
        match message:
            case "** wrong format":
                return
            case "** start time hour should end with :30 or :00":
                print("Time you give should end with :30 or :00.")
                if message2 == "no empty slots":
                    print("This day is full. Try another day.")
                    return
                decision = input(
                    f"would you like to make a reservation for {message2} instead? (yes/no): ")
                if decision != "yes":
                    return
                start_time = message2
                self.make_a_reservation(name, start_time)
                return
            case "** too many reservations this week for the user":
                print("You can't make 3rd reservation this week, sorry.")
                return
            case "** start time out of range":
                print("Start time you provide is when the court is closed.")
                if message2 == "no empty slots":
                    return
                decision = input(
                    f"would you like to make a reservation for {message2} instead? (yes/no): ")
                if decision != "yes":
                    return
                start_time = message2
                self.make_a_reservation(name, start_time)
                return
            case "** reservation less than 1 hour from now":
                print("It's too late to make this reservation.")
                return
            case "** start time occupied":
                print("Your start time is unavailable.")
                if message2 == "no empty slots":
                    print("This day is full. Try another day.")
                    return
                decision = input(
                    f"would you like to make a reservation for {message2} instead? (yes/no): ")
                if decision != "yes":
                    return
                start_time = message2
                self.make_a_reservation(name, start_time)
                return
            case "** time slot occupied":
                print("Reservation for provided time is unavailable.")
                if message2 == "no empty slots":
                    print("This day is full. Try another day.")
                    return
                decision = input(
                    f"would you like to make a reservation for {message2} instead? (yes/no): ")
                if decision != "yes":
                    return
                start_time = message2
                self.make_a_reservation(name, start_time)
                return

        print("How long would you like to book court?")
        halves = self.manager.get_free_halves_qty(start_time)
        for i in range(1, halves + 1):
            print(f"{i}) {i*30} min")
        decision = input("Please type number of chosen option: ")
        if int(decision) <= halves:
            self.manager.make_reservation(
                name, start_time, str(int(decision) * 30))
            print("Reservation made")
        else:
            print("Reservation failed")

    def print_schedule(self):
        start_date = input("Type start date {DD.MM.YYYY HH:MM}: ")
        end_date = input("Type end date {DD.MM.YYYY HH:MM}: ")
        # start_date = "10.02.2023 12:00"
        # end_date = "30.08.2023 12:00"
        self.manager.print_schedule(start_date, end_date)

    def cancel_reservation(self):
        name = input("Please type your first and last name: ")
        date = input("Type start date of a reservation {DD.MM.YYYY HH:MM}: ")
        self.manager.cancel_reservation(name, date)

    def save_schedule(self):
        start_date = input("Type start date {DD.MM.YYYY HH:MM}: ")
        end_date = input("Type end date {DD.MM.YYYY HH:MM}: ")
        filename = input("Type name of the file: ")
        filetype = input("Choose file type (csv / json): ")
        self.manager.save_schedule(start_date, end_date, filetype, filename)

    def run(self):
        while self.status != False:
            self.get_user_input()


hehee = CourtReservationApp()
hehee.run_app()
