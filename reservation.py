from datetime import datetime

class Reservation():
    def __init__(self, reservation) -> None:
        self.name = reservation[0]
        self.start_time = datetime.strptime(reservation[1], r'%d.%m.%Y %H:%M')
        self.end_time = datetime.strptime(reservation[2], r'%d.%m.%Y %H:%M')