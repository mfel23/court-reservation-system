import sqlite3
from data_controller import DataController


class Database():

    def __init__(self):
        self.DB_LOCATION = "court_reservations.db"

    def connect(self):
        self.connection = sqlite3.connect(self.DB_LOCATION)
        self.cur = self.connection.cursor()

    def make_reservation(self, name, start_time, end_time):
        # self.cur.execute("""
        #     INSERT INTO reservations VALUES
        #         ('Michał Najman', '23.03.2023 14:00', '23.03.2023 15:00'),
        #         ('Marcin Wiśniewski', '23.03.2023 17:00', '23.03.2023 18:00'),
        #         ('Tadeusz Stockinger', '24.03.2023 11:00', '24.03.2023 12:30'),
        #         ('Robert Brzozowski', '24.03.2023 12:30', '24.03.2023 14:00'),
        #         ('Norman Dudziuk', '24.03.2023 16:30', '24.03.2023 17:00'),
        #         ('Klaus Sevkovic', '25.03.2023 14:00', '25.03.2023 14:30'),
        #         ('Paweł Gulczyński', '26.03.2023 12:00', '26.03.2023 13:30'),
        #         ('Mieczysław Okniński', '26.03.2023 13:30', '26.03.2023 14:00')
        # """)
        self.connect()
        self.cur.execute(f"""
            INSERT INTO reservations VALUES
                ('{name}', '{start_time}', '{end_time}')
        """)
        self.commit()
        self.close()

    def cancel_reservation(self, name, start_time):
        self.connect()
        self.cur.execute(f"""
            DELETE FROM reservations WHERE name='{name}' AND start_time='{start_time}'
        """)
        self.commit()
        self.close()

    def load_db(self):
        self.connect()
        res = self.cur.execute("SELECT * FROM reservations").fetchall()
        self.close()
        return res

    def close(self):
        self.connection.close()

    def execute(self, new_data):
        self.cur.execute(new_data)

    def create_table(self):
        self.cur.execute('''CREATE TABLE IF NOT EXISTS reservations(name text, \
                                                            start_time text, 
                                                            end_time text)''')

    def commit(self):
        self.connection.commit()

    def get_colnames(self):
        cursor = self.connection.execute('select * from reservations')
        names = [description[0] for description in cursor.description]
        print(names)