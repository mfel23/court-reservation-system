from db_manager import Database
from reservation import Reservation
from data_controller import DataController
from datetime import datetime, date, timedelta
from itertools import groupby
import csv
import json


class ReservationsManager:
    def __init__(self) -> None:
        self.db = Database()
        self.load_reservations()
        self.min_hour = 12
        self.max_hour = 18

    def load_reservations(self):
        reservations_loaded = self.db.load_db()
        self.reservations = []
        for reservation in reservations_loaded:
            self.reservations.append(Reservation(reservation))

    def make_reservation(self, name, start_time, minutes):
        end_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M") + timedelta(
            minutes=int(minutes)
        )
        end_time = end_time.strftime(r"%d.%m.%Y %H:%M")
        self.db.make_reservation(name, start_time, end_time)
        self.load_reservations()

    def check_conditions_before_reservation(self, name, start_time):
        self.load_reservations()
        if not DataController.check_format(name, start_time):
            return "** wrong format", None
        if not self.is_start_time_correct(start_time):
            start_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M")
            start_time = f"{start_time.strftime(r'%d.%m.%Y %H')}:00"
            return (
                "** start time hour should end with :30 or :00",
                self.find_closest_free_start_time(start_time),
            )
        if self.check_qty_user_reservations(name, start_time) > 2:
            return "** too many reservations this week for the user", None
        if not self.start_time_within_court_open_time(start_time):
            return "** start time out of range", self.find_closest_free_start_time(
                start_time
            )
        if self.is_start_too_early(start_time):
            return "** reservation less than 1 hour from now", None
        if not self.is_start_time_empty(start_time):
            return "** start time occupied", self.find_closest_free_start_time(
                start_time
            )
        if self.get_free_halves_qty(start_time) == 0:
            return "** time slot occupied", self.find_closest_free_start_time(
                start_time
            )
        return "*** all conditions OK", None

    def start_time_within_court_open_time(self, start_time):
        start_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M")
        if start_time.hour < self.min_hour or start_time.hour > self.max_hour - 1:
            return False
        return True

    def is_start_time_correct(self, start_time):
        start_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M")
        if start_time.minute == 30 or start_time.minute == 0:
            return True
        else:
            return False

    def check_qty_user_reservations(self, name, start_time):
        start_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M")
        quantity = len(
            [
                reservation.start_time.hour
                for reservation in self.reservations
                if reservation.name == name
                and reservation.start_time.strftime(r"%W") == start_time.strftime(r"%W")
            ]
        )
        return quantity

    def cancel_reservation(self, name, start_time):
        self.load_reservations()
        if not DataController.check_format(name, start_time):
            return
        if not self.does_reservation_exist(name, start_time):
            print("** Reservation you try to cancel does not exist.")
            return
        if self.is_start_too_early(start_time):
            print(
                "** Reservation you try to cancel is less than one hour form now.\nIt can't be cancelled."
            )
            return
        self.db.cancel_reservation(name, start_time)
        print("Reservation cancelled")
        self.load_reservations()

    def print_schedule(self, start_time, end_time):
        if not DataController.check_format("Fake Name", start_time, end_time):
            return
        self.load_reservations()
        longest_name = max(
            [reservation.name for reservation in self.reservations], key=len
        )
        start_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M")
        end_time = datetime.strptime(end_time, r"%d.%m.%Y %H:%M")
        filtered_reservations = [
            reservation
            for reservation in self.reservations
            if start_time <= reservation.start_time and end_time >= reservation.end_time
        ]
        sorted_reservations = sorted(
            filtered_reservations, key=lambda reservation: reservation.start_time
        )
        grouped = {
            key: list(result)
            for key, result in groupby(
                sorted_reservations, key=lambda reservation: reservation.start_time.year
            )
        }
        for year, year_reservations in grouped.items():
            grouped_months = {
                key: list(result)
                for key, result in groupby(
                    year_reservations,
                    key=lambda reservation: reservation.start_time.month,
                )
            }
            for month, month_reservations in grouped_months.items():
                grouped_days = {
                    key: list(result)
                    for key, result in groupby(
                        month_reservations,
                        key=lambda reservation: reservation.start_time.day,
                    )
                }
                grouped_months[month] = grouped_days
            grouped[year] = grouped_months

        for year, year_dict in grouped.items():
            for month, month_dict in year_dict.items():
                for day, day_reservations in month_dict.items():
                    print("")
                    print(f"{day}.{month}.{year}:")
                    for reservation in day_reservations:
                        gap = (len(longest_name) - len(reservation.name)) * " "
                        print(
                            f"* {reservation.name}{gap} {reservation.start_time.strftime(r'%d.%m.%Y %H:%M')} - {reservation.end_time.strftime(r'%d.%m.%Y %H:%M')}"
                        )

    def prepare_json(self, start_time, end_time):
        self.load_reservations()
        filtered_reservations = [
            reservation
            for reservation in self.reservations
            if start_time <= reservation.start_time and end_time >= reservation.end_time
        ]
        sorted_reservations = sorted(
            filtered_reservations, key=lambda reservation: reservation.start_time
        )
        grouped = {
            key: list(result)
            for key, result in groupby(
                sorted_reservations, key=lambda reservation: reservation.start_time.year
            )
        }
        for year, year_reservations in grouped.items():
            grouped_months = {
                key: list(result)
                for key, result in groupby(
                    year_reservations,
                    key=lambda reservation: reservation.start_time.month,
                )
            }
            for month, month_reservations in grouped_months.items():
                grouped_days = {
                    key: list(result)
                    for key, result in groupby(
                        month_reservations,
                        key=lambda reservation: reservation.start_time.day,
                    )
                }
                for day, day_reservations in grouped_days.items():
                    reservations_list = []
                    for reservation in day_reservations:
                        reservations_list.append(
                            {
                                "name": reservation.name,
                                "start_time": reservation.start_time.strftime("%H:%M"),
                                "end_time": reservation.end_time.strftime("%H:%M"),
                            }
                        )
                    grouped_days[day] = reservations_list
                grouped_months[month] = grouped_days
            grouped[year] = grouped_months
        return grouped

    def save_schedule(self, start_time, end_time, filetype, filename):
        if not DataController.check_format("Fake Name", start_time, end_time):
            return
        start_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M")
        end_time = datetime.strptime(end_time, r"%d.%m.%Y %H:%M")
        filtered_reservations = [
            reservation
            for reservation in self.reservations
            if start_time <= reservation.start_time and end_time >= reservation.end_time
        ]
        sorted_reservations = sorted(
            filtered_reservations, key=lambda reservation: reservation.start_time
        )

        if filetype == "csv":
            with open(f"{filename}.csv", "w", encoding="UTF8") as f:
                writer = csv.writer(f)
                writer.writerow(["name", "start_time", "end_time"])
                for reservation in sorted_reservations:
                    row = [
                        reservation.name,
                        reservation.start_time.strftime(r"%d.%m.%Y %H:%M"),
                        reservation.end_time.strftime(r"%d.%m.%Y %H:%M"),
                    ]
                    writer.writerow(row)
        elif filetype == "json":
            grouped = self.prepare_json(start_time, end_time)
            with open(f"{filename}.json", "w", encoding="UTF8") as outfile:
                json.dump(grouped, outfile, ensure_ascii=False)
        else:
            print(
                "** Given type of the file you want to save is wrong.\n** Choose csv or json."
            )

    def print_reservations(self):
        longest_name = max(
            [reservation.name for reservation in self.reservations], key=len
        )
        for reservation in self.reservations:
            gap = len(longest_name) - len(reservation.name)
            print(
                reservation.name,
                gap * " ",
                " | ",
                reservation.start_time,
                " | ",
                reservation.end_time,
            )

    def does_reservation_exist(self, name, start_time):
        self.load_reservations()
        start_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M")
        reservation = [
            reservation
            for reservation in self.reservations
            if reservation.name == name and reservation.start_time == start_time
        ]
        if len(reservation) == 1:
            return True
        else:
            return False

    def is_start_time_empty(self, start_time):
        self.load_reservations()
        start_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M")
        for reservation in self.reservations:
            if (
                start_time >= reservation.start_time
                and start_time < reservation.end_time
            ):
                return False
        return True

    def weekly_reservations_limit_exceeded(self, name, week) -> bool:
        names = [
            reservation.name
            for reservation in self.reservations
            if reservation.start_time.isocalendar().week == week
        ]
        if names.count(name) >= 2:
            return True
        else:
            return False

    def is_start_too_early(self, start_time) -> bool:
        if datetime.strptime(
            start_time, r"%d.%m.%Y %H:%M"
        ) >= datetime.now() + timedelta(hours=1):
            return False
        else:
            return True

    def get_free_halves_qty(self, start_time) -> int:
        start_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M")
        filtered_reservations = [
            reservation
            for reservation in self.reservations
            if start_time < reservation.start_time
        ]
        if len(filtered_reservations) < 1:
            filtered_reservations.append(
                Reservation(
                    [
                        "fake reservation",
                        f"{start_time.strftime(r'%d.%m.%Y')} 18:00",
                        "12.12.2023 23:00",
                    ]
                )
            )
        next_reservation = sorted(
            filtered_reservations, key=lambda reservation: reservation.start_time
        )[0]
        empty_timeslot = (next_reservation.start_time - start_time).total_seconds() / 60
        free_halves = int(empty_timeslot / 30)
        if free_halves >= 3:
            free_halves = 3
        return free_halves

    def find_closest_free_start_time(self, start_time) -> str:
        start_time = datetime.strptime(start_time, r"%d.%m.%Y %H:%M")
        start_day = start_time.day
        start_month = start_time.month
        while (
            not self.is_start_time_empty(start_time.strftime(r"%d.%m.%Y %H:%M"))
            or self.get_free_halves_qty(start_time.strftime(r"%d.%m.%Y %H:%M")) == 0
            or self.is_start_too_early(start_time.strftime(r"%d.%m.%Y %H:%M"))
            or not start_time.day == start_day
            or start_time.hour >= self.max_hour
            or start_time.hour < self.min_hour
        ):
            start_time += timedelta(minutes=30)
        if start_time.day != start_day:
            while (
                not self.is_start_time_empty(start_time.strftime(r"%d.%m.%Y %H:%M"))
                or self.get_free_halves_qty(start_time.strftime(r"%d.%m.%Y %H:%M")) == 0
                or self.is_start_too_early(start_time.strftime(r"%d.%m.%Y %H:%M"))
                or not start_time.day == start_day
                or start_time.hour >= self.max_hour
                or start_time.hour < self.min_hour
            ):
                start_time -= timedelta(minutes=30)
        if start_time.month != start_month:
            return "no empty slots"
        return start_time.strftime(r"%d.%m.%Y %H:%M")
