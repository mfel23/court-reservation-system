from datetime import datetime


class DataController():

    def __init__(self) -> None:
        pass

    @classmethod
    def check_format(cls, name, *args):
        try:
            if not len(name.split(" ")) == 2:
                raise ValueError("Please provide first and last name")
            if not isinstance(name, str):
                raise TypeError("First and last name should be string type")
            for n in name.split(" "):
                if n.isdecimal():
                    raise TypeError(
                        "First and last name has to be string type, not numeric")
            for arg in args:
                if not isinstance(arg, str) or not isinstance(datetime.strptime(arg, r'%d.%m.%Y %H:%M'), datetime):
                    raise ValueError("Wrong date and time format.")
        except ValueError as e:
            print(f"** {e.args[0]}")
            return False
        except TypeError as e:
            print(f"** {e.args[0]}")
            return False
        return True
