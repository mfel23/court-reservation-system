import unittest
from data_controller import DataController
class TestFormat(unittest.TestCase):
    def test_single_word(self):
        self.assertFalse(DataController.check_format("Mateusz"))
    def test_two_words(self):
        self.assertTrue(DataController.check_format("Mateusz Felczak"))
    def test_two_numbers(self):
        self.assertFalse(DataController.check_format("1 1"))
    def test_number_and_space(self):
        self.assertFalse(DataController.check_format("1 "))
    def test_correct_datetime(self):
        self.assertTrue(DataController.check_format("Mateusz Felczak", "12.03.2023 12:00"))
    def test_incorrect_datetime(self):
        self.assertFalse(DataController.check_format("Mateusz Felczak", "12.03.2023 12"))
if __name__ == '__main__':
    unittest.main()