# REPL court reservation system

## Used libraries:
* datetime -> because this reservation system is based on date and time, and with datetime it is easy to compare datetime objects,
* convert from and to other types like string etc.
* sqlite3 -> data storage
* csv, json -> writing content to csv and json files
* itertools -> grouping reservations for schedule printout and saving schedule to json

## How to use the script:
* At first, run court_reservation_app.py
* Then you can type "help" and all available commands will be listed.
* Try them all! ;)

## Few basic scenarios:

* 1) make a reservation
    ```
    $ Type what you want to do (type help for more information): 1
    $ Please type your first and last name: Mateusz Felczak
    $ When would you like to book? {DD.MM.YYYY HH:MM}: 28.03.2023 11:30
    $ Start time you provide is when the court is closed.
    $ would you like to make a reservation for 28.03.2023 12:00 instead? (yes/no): yes
    $ How long would you like to book court?
    $ 1) 30 min
    $ 2) 60 min
    $ 3) 90 min
    $ Please type number of chosen option: 3
    $ Reservation made
    ```
    
* 2) Cancel a reservation
    ```
    $ Type what you want to do (type help for more information): 2
    $ Please type your first and last name: Mateusz Felczak
    $ Type start date of a reservation {DD.MM.YYYY HH:MM}: 28.03.2023 12:00
    $ Reservation cancelled
    ```

* 3) Print a schedule
    ```
    $ Type what you want to do (type help for more information): 3
    $ Type start date {DD.MM.YYYY HH:MM}: 10.02.2023 12:00
    $ Type end date {DD.MM.YYYY HH:MM}: 12.12.2023 12:00
    ```

    22.3.2023:
    * Mat Fel             22.03.2023 12:00 - 22.03.2023 13:00

    23.3.2023:
    * Michał Najman       23.03.2023 14:00 - 23.03.2023 15:00
    * Marcin Wiśniewski   23.03.2023 17:00 - 23.03.2023 18:00

    24.3.2023:
    * Tadeusz Stockinger  24.03.2023 11:00 - 24.03.2023 12:30
    * Robert Brzozowski   24.03.2023 12:30 - 24.03.2023 14:00
    * Norman Dudziuk      24.03.2023 16:30 - 24.03.2023 17:00

    25.3.2023:
    * Klaus Sevkovic      25.03.2023 14:00 - 25.03.2023 14:30

    26.3.2023:
    * Paweł Gulczyński    26.03.2023 12:00 - 26.03.2023 13:30
    * Mieczysław Okniński 26.03.2023 13:30 - 26.03.2023 14:00

* 4) Save schedule to csv / json file

    ```
    $ Type what you want to do (type help for more information): 4
    $ Type start date {DD.MM.YYYY HH:MM}: 23.03.2023 11:00
    $ Type end date {DD.MM.YYYY HH:MM}: 24.03.2023 23:00
    $ Type name of the file: json
    $ Choose file type (csv / json): json
    ```

    Output file:
    {
    "2023": {
        "3": {
        "23": [
            { "name": "Michał Najman", "start_time": "14:00", "end_time": "15:00" },
            {
            "name": "Marcin Wiśniewski",
            "start_time": "17:00",
            "end_time": "18:00"
            }
        ],
        "24": [
            {
            "name": "Tadeusz Stockinger",
            "start_time": "11:00",
            "end_time": "12:30"
            },
            {
            "name": "Robert Brzozowski",
            "start_time": "12:30",
            "end_time": "14:00"
            },
            { "name": "Norman Dudziuk", "start_time": "16:30", "end_time": "17:00" }
        ]
        }
    }
    }

* 5) Exit